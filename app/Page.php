<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->timestamps();
        });
    }
    public static function getuserData(){
        $value=DB::table('users')->orderBy('id', 'asc')->get();
        return $value;
      }
    
}
