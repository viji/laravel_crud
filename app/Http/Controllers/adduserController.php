<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class adduserController extends Controller
{
    public function adduserpage(){
 
        // Read value from Model method
        //$userData = Page::getuserData();
    
        // Pass to view
        return view('adduser');
      }
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);

        $user = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]);
        $user->save();
        return redirect('/')->with('success', 'User added sucessfully!');
    }
    public function edituserpage($id)
    {
        $edituserval = User::find($id);
        return view('edituser')->with("edituserval",$edituserval);        
    }
    public function updateuser(Request $request, $id)
    {
        

        $user = User::find($id);
        $user->name =  $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->save();

        return redirect('/')->with('success', 'User updated!');
    }
    public function deleteuser($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/')->with('success', 'user deleted!');
    }
    public function viewuser($id)
    {
        $user = User::find($id);
        return view('viewuser')->with("user",$user);
        //return response(['user' => new ProjectResource($user), 'message' => 'retrieved'], 201);
        
    }
}
