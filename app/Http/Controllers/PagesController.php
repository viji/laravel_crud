<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\User;

class PagesController extends Controller{
 
  public function index(){
 
    // Read value from Model method
    $userData = Page::getuserData();

    // Pass to view
    //return view('welcome')->with("userData",$userData);
    return $userData;
    
  }
  public function viewuser($id)
    {
        $user = User::find($id);
        //return view('viewuser')->with("user",$user);
        return response([$user, 'message' => 'retrieved'], 201);
        
    }
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);

        $user = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]);
        $user->save();
        return response(['message' => 'retrieved'], 201);
    }
    public function deleteuser($id)
    {
        $user = User::find($id);
        $user->delete();

        return response(['message' => 'deleted'], 201);
    }

}