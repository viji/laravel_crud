@extends('base')

<div class="container">
  <h4>View User</h4>        
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>{{ $user->id }}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Email</td>
        <td>{{ $user->email }}</td>
      </tr>
    </tbody>
  </table>
</div>
