@extends('base')
  


<div class="container">
<h4>Add user</h4>
<a href="/" class="btn btn-danger" style="float:right">Back</a><br><br>
@if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif

    

    <form method="POST" action="/adduservalues">

        {{ csrf_field() }}

       <div class="form-group">
          <label >Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control">

      </div>
      <div class="form-group">
            <label >Email</label>
            <input type="text" name="email" placeholder="email" class="form-control">

      </div>
      <div class="form-group">
            <label >Password</label>
            <input type="password" name="password" placeholder="password" class="form-control">

      </div>
      <div class="form-group">

            <button type="submit" class="btn btn-primary">Add</button>
            <a href="/clearform" class="btn btn-danger">Clear</a>

      </div>

    </form>  

 </div>   