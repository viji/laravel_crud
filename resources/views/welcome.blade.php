@extends('base')
    <div class="container">
  <h3>User management</h3><br>
  <a href="/adduser" class="btn btn-primary">Add User</a><br><br>
  @if(session()->get('success'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{ session()->get('success') }}  
    </div>
  @endif    
  <table class="table">
    <thead>
      <tr>
      <th>id</th>
        <th>Name</th>
        <th>Email</th>
        <th colspan=2>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($userData as $user)
      <tr>
      <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>
                <a href="/edituser/{{$user->id}}" class="btn btn-success">Edit</a>
                <a href="/viewuservalues/{{$user->id}}" class="btn btn-primary">View</a>
                </td>
                <td>
                <form style="display: inline;" method="POST" action="/deleteuservalues/{{$user->id}}">
                        <button onclick="return confirm('Are you sure?');" class="btn btn-danger" type="submit">Delete</button>
                        @method('DELETE')
                        {{ csrf_field() }} 
                </form>
                
                </td>
                
         </td> 
      </tr>
     
    </tbody>
    @endforeach
  </table>
</div>