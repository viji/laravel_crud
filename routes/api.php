<?php

use Illuminate\Http\Request;
use App\Http\Controllers\PagesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('uservalues',[PagesController::class,'index']);
Route::get('/viewuservalues/{id}',[PagesController::class,'viewuser']);
Route::post('/adduservalues',[PagesController::class,'store']);
Route::delete('/deleteuser/{id}',[PagesController::class,'deleteuser']);