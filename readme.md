

**Create new project command:**

- $ composer create-project laravel/laravel your-project-name

**Run command:**

- $ php artisan serve

**Create table using migration:**

- $ php artisan migrate:make create_users_table

**Migration command:**

- $ php artisan migrate

**Create controller:**

- $ php artisan make:controller controller_name

**Create model:**

- $ php artisan make:model model_name

**Create a view 'index.blade.php' in the default directory:**

- $ php artisan make:view index

**Create a view 'index.blade.php' in a subdirectory ('pages'):**

- $ php artisan make:view pages.index

**Create a view with a different file extension ('index.html'):**

- $ php artisan make:view index --extension=html


**Install Laravel PHP Framework on Ubuntu 16.04 | 17.10 | 18.04 with Apache2 and PHP 7.2 Support**

Refer:-https://websiteforstudents.com/install-laravel-php-framework-on-ubuntu-16-04-17-10-18-04-with-apache2-and-php-7-2-support/
